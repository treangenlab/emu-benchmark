name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Lactobacillus paragasseri	2107999	S	52	5	57	0.00039
Homo sapiens	9606	S	145178	67	145245	0.98783
Lactobacillus crispatus	47770	S	96	1	97	0.00067
Gardnerella vaginalis	2702	S	658	8	666	0.00453
Candida albicans	5476	S	47	3	50	0.00034
Lactobacillus jensenii	109790	S	88	0	88	0.00060
Lactobacillus gasseri	1596	S	703	92	795	0.00541
Atopobium vaginae	82135	S	20	0	20	0.00014
Lactobacillus iners	147802	S	14	0	14	0.00010
