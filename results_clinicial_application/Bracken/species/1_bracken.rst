name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Homo sapiens	9606	S	167468	137	167605	0.99081
Lactobacillus iners	147802	S	1334	0	1334	0.00789
Gardnerella vaginalis	2702	S	48	4	52	0.00031
Cutibacterium acnes	1747	S	21	2	23	0.00014
Lactobacillus crispatus	47770	S	92	8	100	0.00059
Corynebacterium jeikeium	38289	S	32	13	45	0.00027
