name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Emiliania huxleyi	2903	S	13	0	13	0.00002
Prevotella ihumii	1917878	S	11	0	11	0.00002
Lactobacillus crispatus	47770	S	190	11	201	0.00032
Atopobium vaginae	82135	S	264	3	267	0.00042
Lactobacillus paragasseri	2107999	S	22	10	32	0.00005
Sneathia amnii	187101	S	15	0	15	0.00002
Prevotella bivia	28125	S	17	1	18	0.00003
Lactobacillus coleohominis	181675	S	12	0	12	0.00002
Rasamsonia emersonii	68825	S	11	0	11	0.00002
Prevotella timonensis	386414	S	23	1	24	0.00004
Sneathia sanguinegens	40543	S	17	0	17	0.00003
Homo sapiens	9606	S	623898	618	624516	0.99118
Gardnerella vaginalis	2702	S	2669	30	2699	0.00428
Aerococcus christensenii	87541	S	78	9	87	0.00014
Megasphaera genomosp. type_1	699192	S	260	16	276	0.00044
Lactobacillus gasseri	1596	S	11	10	21	0.00003
Toxoplasma gondii	5811	S	18	0	18	0.00003
Peptoniphilus harei	54005	S	10	7	17	0.00003
Lactobacillus iners	147802	S	1614	0	1614	0.00256
Phycomyces blakesleeanus	4837	S	11	1	12	0.00002
Bifidobacterium dentium	1689	S	11	0	11	0.00002
Prevotella amnii	419005	S	162	6	168	0.00027
Tetrahymena thermophila	5911	S	11	1	12	0.00002
