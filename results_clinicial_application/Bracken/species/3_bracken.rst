name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Homo sapiens	9606	S	243981	102	244083	0.99149
Lactobacillus iners	147802	S	14	0	14	0.00006
Lactobacillus helveticus	1587	S	24	4	28	0.00011
Gardnerella vaginalis	2702	S	64	7	71	0.00029
Lactobacillus jensenii	109790	S	44	0	44	0.00018
Prevotella bivia	28125	S	11	1	12	0.00005
Lactobacillus crispatus	47770	S	1878	47	1925	0.00782
