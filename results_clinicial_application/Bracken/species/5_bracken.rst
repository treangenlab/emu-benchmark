name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Emiliania huxleyi	2903	S	16	0	16	0.00003
Prevotella timonensis	386414	S	11	0	11	0.00002
Cutibacterium acnes	1747	S	13	2	15	0.00003
Lactobacillus iners	147802	S	14	0	14	0.00003
Homo sapiens	9606	S	477077	220	477297	0.98537
Lactobacillus crispatus	47770	S	6129	132	6261	0.01293
Lactobacillus helveticus	1587	S	536	32	568	0.00117
Lactobacillus amylovorus	1604	S	29	24	53	0.00011
Gardnerella vaginalis	2702	S	78	6	84	0.00018
Tetrahymena thermophila	5911	S	11	0	11	0.00002
Lactobacillus gigeriorum	1203069	S	50	0	50	0.00010
