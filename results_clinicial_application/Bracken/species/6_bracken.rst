name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Lactobacillus crispatus	47770	S	1983	45	2028	0.02355
Lactobacillus helveticus	1587	S	12	2	14	0.00017
Lactobacillus jensenii	109790	S	19	0	19	0.00022
Gardnerella vaginalis	2702	S	44	2	46	0.00053
Homo sapiens	9606	S	83998	43	84041	0.97553
