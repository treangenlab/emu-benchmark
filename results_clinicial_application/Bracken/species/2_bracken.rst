name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Prevotella timonensis	386414	S	11	1	12	0.00004
Lactobacillus vaginalis	1633	S	84	5	89	0.00029
Homo sapiens	9606	S	300010	195	300205	0.98554
Lactobacillus crispatus	47770	S	2691	72	2763	0.00907
Lactobacillus helveticus	1587	S	47	6	53	0.00018
Gardnerella vaginalis	2702	S	91	1	92	0.00030
Aerococcus christensenii	87541	S	17	9	26	0.00009
Lactobacillus jensenii	109790	S	73	0	73	0.00024
Atopobium vaginae	82135	S	17	0	17	0.00006
Cutibacterium acnes	1747	S	19	3	22	0.00007
Lactobacillus iners	147802	S	1254	0	1254	0.00412
