name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Emiliania huxleyi	2903	S	17	0	17	0.00002
Peptostreptococcus anaerobius	1261	S	132	6	138	0.00018
Colletotrichum graminicola	31870	S	11	4	15	0.00002
Botrytis cinerea	40559	S	11	3	14	0.00002
Atopobium vaginae	82135	S	24	0	24	0.00003
Sneathia amnii	187101	S	14	0	14	0.00002
Lactobacillus helveticus	1587	S	38	6	44	0.00006
Trypanosoma conorhini	83891	S	11	0	11	0.00001
Prevotella bivia	28125	S	471	20	491	0.00063
Lactobacillus crispatus	47770	S	3607	148	3755	0.00481
Rasamsonia emersonii	68825	S	12	0	12	0.00002
Prevotella timonensis	386414	S	15	0	15	0.00002
Homo sapiens	9606	S	773525	583	774108	0.99231
Leptosphaeria maculans	5022	S	10	29	39	0.00005
Gardnerella vaginalis	2702	S	164	5	169	0.00022
Cavenderia fasciculata	261658	S	10	0	10	0.00001
Megasphaera genomosp. type_1	699192	S	15	1	16	0.00002
Peptoniphilus harei	54005	S	24	6	30	0.00004
Salmonella enterica	28901	S	11	14	25	0.00003
Aerococcus christensenii	87541	S	10	1	11	0.00002
Lactobacillus iners	147802	S	37	0	37	0.00005
Phycomyces blakesleeanus	4837	S	10	5	15	0.00002
Bifidobacterium bifidum	1681	S	36	7	43	0.00006
Bifidobacterium dentium	1689	S	827	13	840	0.00108
Toxoplasma gondii	5811	S	16	0	16	0.00002
Tetrahymena thermophila	5911	S	10	0	10	0.00001
Atopobium rimae	1383	S	185	0	185	0.00024
