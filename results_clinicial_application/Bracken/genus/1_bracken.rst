name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Staphylococcus	1279	G	11	3	14	0.00008
Homo	9605	G	167468	137	167605	0.99005
Streptococcus	1301	G	22	7	29	0.00018
Prevotella	838	G	14	0	14	0.00008
Gardnerella	2701	G	48	2	50	0.00030
Lactobacillus	1578	G	1450	0	1450	0.00857
Cutibacterium	1912216	G	25	0	25	0.00015
Aspergillus	5052	G	15	13	28	0.00017
Corynebacterium	1716	G	70	3	73	0.00043
