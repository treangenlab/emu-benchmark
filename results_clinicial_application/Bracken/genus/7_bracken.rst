name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Streptococcus	1301	G	30	3	33	0.00001
Cordyceps	45234	G	13	0	13	0.00001
Sclerotinia	5179	G	16	0	16	0.00001
Cladophialophora	82105	G	26	0	26	0.00001
Paenibacillus	44249	G	15	0	15	0.00001
Pestalotiopsis	37840	G	10	1	11	0.00000
Mycoplasma	2093	G	19	0	19	0.00001
Fusarium	5506	G	51	9	60	0.00002
Paracoccidioides	38946	G	13	9	22	0.00001
Rhinocladiella	5587	G	10	0	10	0.00000
Exophiala	5583	G	56	0	56	0.00002
Anaerococcus	165779	G	45	5	50	0.00002
Heterobasidion	13562	G	12	0	12	0.00000
Aerococcus	1375	G	1464	56	1520	0.00056
Gemella	1378	G	10	0	10	0.00000
Bacteroides	816	G	19	0	19	0.00001
Pseudomonas	286	G	14	22	36	0.00001
Dialister	39948	G	22	1	23	0.00001
Chaetomium	5149	G	12	0	12	0.00000
Sordaria	5146	G	11	0	11	0.00000
Gardnerella	2701	G	7853	48	7901	0.00291
Neurospora	5140	G	11	1	12	0.00000
Acanthamoeba	5754	G	32	0	32	0.00001
Entamoeba	5758	G	15	0	15	0.00001
Sphaerulina	237179	G	16	10	26	0.00001
Blastomyces	229219	G	23	1	24	0.00001
Trypanosoma	5690	G	94	1	95	0.00003
Rhizopus	4842	G	11	13	24	0.00001
Bacillus	1386	G	64	4	68	0.00003
Atopobium	1380	G	154	2	156	0.00006
Lachnellula	47830	G	22	0	22	0.00001
Fusobacterium	848	G	11	0	11	0.00000
Emiliania	2902	G	70	0	70	0.00003
Guillardia	55528	G	15	0	15	0.00001
Clostridium	1485	G	61	5	66	0.00002
Corynebacterium	1716	G	13	2	15	0.00001
Byssochlamys	5092	G	12	3	15	0.00001
Vanderwaltozyma	374469	G	10	1	11	0.00000
Talaromyces	5094	G	18	3	21	0.00001
Tuber	36048	G	27	0	27	0.00001
Rhizophagus	1129544	G	29	0	29	0.00001
Leptomonas	5683	G	28	0	28	0.00001
Fonsecaea	40354	G	27	0	27	0.00001
Plasmodium	5820	G	100	2	102	0.00004
Jaminaea	561108	G	21	0	21	0.00001
Legionella	445	G	13	0	13	0.00000
Marssonina	324777	G	30	0	30	0.00001
Megasphaera	906	G	32	1	33	0.00001
Arthrobacter	1663	G	12	0	12	0.00000
Sneathia	168808	G	43	0	43	0.00002
Perkinsus	28000	G	15	0	15	0.00001
Clavispora	36910	G	12	0	12	0.00000
Winkia	2692118	G	693	7	700	0.00026
Finegoldia	150022	G	61	4	65	0.00002
Lobosporangium	299330	G	18	0	18	0.00001
Cyclospora	44417	G	17	0	17	0.00001
Blastocystis	12967	G	14	0	14	0.00001
Trichoderma	5543	G	28	0	28	0.00001
Dictyostelium	5782	G	10	0	10	0.00000
Bifidobacterium	1678	G	27	1	28	0.00001
Cupriavidus	106589	G	11	0	11	0.00000
Pyricularia	48558	G	13	0	13	0.00000
Leptosphaeria	5021	G	18	0	18	0.00001
Bipolaris	33194	G	22	0	22	0.00001
Botrytis	33196	G	45	7	52	0.00002
Tetrahymena	5890	G	44	1	45	0.00002
Trichophyton	5550	G	11	0	11	0.00000
Leishmania	5658	G	44	0	44	0.00002
Capronia	43220	G	30	11	41	0.00002
Amorphotheca	5100	G	15	0	15	0.00001
Pseudocercospora	131324	G	10	0	10	0.00000
Phycomyces	4836	G	35	1	36	0.00001
Candida	1535326	G	52	15	67	0.00003
Toxoplasma	5810	G	105	1	106	0.00004
Olegusella	1979843	G	10	1	11	0.00000
Puccinia	5296	G	11	2	13	0.00000
Kwoniella	490731	G	14	2	16	0.00001
Paramecium	5884	G	19	0	19	0.00001
Arthrobotrys	13348	G	15	2	17	0.00001
Flavobacterium	237	G	26	0	26	0.00001
Aspergillus	5052	G	231	169	400	0.00015
Acytostelium	133407	G	42	0	42	0.00002
Colletotrichum	5455	G	45	3	48	0.00002
Hyaloscypha	47747	G	13	0	13	0.00000
Exserohilum	91493	G	23	0	23	0.00001
Eimeria	5800	G	45	3	48	0.00002
Cavenderia	2058187	G	35	0	35	0.00001
Staphylococcus	1279	G	23	0	23	0.00001
Chryseobacterium	59732	G	19	0	19	0.00001
Homo	9605	G	2698547	2431	2700978	0.99404
Rasamsonia	1132856	G	37	0	37	0.00001
Malassezia	55193	G	11	0	11	0.00000
Streptomyces	1883	G	25	0	25	0.00001
Pseudogymnoascus	78156	G	13	0	13	0.00000
Ureaplasma	2129	G	18	0	18	0.00001
Lactobacillus	1578	G	1504	2	1506	0.00055
Peptoniphilus	162289	G	224	18	242	0.00009
Prevotella	838	G	1194	21	1215	0.00045
