name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Gardnerella	2701	G	44	1	45	0.00052
Prevotella	838	G	12	1	13	0.00015
Lactobacillus	1578	G	2080	2	2082	0.02416
Homo	9605	G	83998	43	84041	0.97517
