name	taxonomy_id	taxonomy_lvl	kraken_assigned_reads	added_reads	new_est_reads	fraction_total_reads
Gardnerella	2701	G	64	7	71	0.00029
Prevotella	838	G	26	0	26	0.00011
Lactobacillus	1578	G	2026	7	2033	0.00826
Homo	9605	G	243981	102	244083	0.99120
Aspergillus	5052	G	15	21	36	0.00015
