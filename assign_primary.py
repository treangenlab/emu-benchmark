import os
import argparse
import pathlib
import pandas as pd
import pysam

emu_names_path = "/home/Users/kdc10/emu_databases/emu_db_v1/names_df.tsv"
emu_nodes_path = "/home/Users/kdc10/emu_databases/emu_db_v1/nodes_df.tsv"
#emu_names_path = "/home/Users/kdc10/emu_databases/emu_db_rdp_v1/names_df.tsv"
#emu_nodes_path = "/home/Users/kdc10/emu_databases/emu_db_rdp_v1/nodes_df.tsv"
output_dir = "results_primary/"

def lineage_dict_from_tid(taxid, nodes_dict, names_dict):
    """Retrieve dict of lineage for given taxid

        tid(int): tax id to retrieve lineage dict
        nodes_df(df): pandas df of nodes.dmp with columns ['tax_id', 'parent_tax_id', 'rank'];
                            tax_id as index
        names_df(df): pandas df of names.dmp with columns ['tax_id', 'name_txt']; tax_id as index
        return {str:str}: dict[rank]:taxonomy name at rank
    """

    lineage_dict = {}
    while names_dict[taxid] != "root":
        tup = nodes_dict[taxid]
        lineage_dict[tup[1]] = names_dict[taxid]
        taxid = tup[0]
    return lineage_dict



def freq_to_lineage_df(counts, tsv_output_path, nodes_dict, names_dict):
    """Converts freq to a pandas df where each row contains abundance and tax lineage for
                classified species in f.keys(). Stores df as .tsv file in tsv_output_path.

        freq{int:float}: dict[species_tax_id]:estimated likelihood species is present in sample
        tsv_output_path(str): path and name of output .tsv file for generated dataframe
        nodes_df(df): pandas df of nodes.dmp with columns ['tax_id', 'parent_tax_id', 'rank'];
                            tax_id as index
        names_df(df): pandas df of names.dmp with columns ['tax_id', 'name_txt']; tax_id as index
        returns(df): pandas df with lineage and abundances for values in f
    """
    total = sum(f_counts.values())
    freq = [v/total for v in counts.values()]
    results_df = pd.DataFrame(list(zip(counts.keys(), counts.values(), freq)),
                              columns=["tax_id", "count", "abundance"])
    lineages = results_df["tax_id"].apply(lambda x: lineage_dict_from_tid(x, nodes_dict, names_dict))
    results_df = pd.concat([results_df, pd.json_normalize(lineages)], axis=1)
    header_order = ["abundance", "species", "genus", "family", "order", "class",
                    "phylum", "clade", "superkingdom", "subspecies",
                    "species subgroup", "species group", "tax_id", "count"]
    for col in header_order:
        if col not in results_df.columns:
            results_df[col] = ""
    results_df = results_df.sort_values(header_order[8:0:-1]).reset_index(drop=True)
    results_df = results_df.reindex(header_order, axis=1)

    results_df.to_csv(f"{tsv_output_path}.tsv", index=False, sep='\t')
    return results_df

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'input_file', type=str,
        help='filepath to input [fasta,fastq,sam]')
    args = parser.parse_args()

    # initialize values
    names_df = pd.read_csv(emu_names_path, sep='\t', dtype=str)
    nodes_df = pd.read_csv(emu_nodes_path, sep='\t', dtype=str)
    dict_names = dict(zip(names_df['tax_id'], names_df['name_txt']))
    dict_names['-'] = '-'
    dict_nodes = dict(zip(nodes_df['tax_id'], tuple(zip(nodes_df['parent_tax_id'], nodes_df['rank']))))
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    filename = pathlib.PurePath(args.input_file).stem
    filetype = pathlib.PurePath(args.input_file).suffix
    pwd = os.getcwd()
    sam_file = f"{args.input_file}"

    # initialize
    f_counts, assignments = {}, []

    # add one count for each primary alignment
    samfile = pysam.AlignmentFile(sam_file)
    for align in samfile.fetch():
        if not align.is_secondary and not align.is_supplementary and align.reference_name:
            tid = align.reference_name.split(":")[0]
            if tid not in f_counts.keys():
                f_counts[tid] = 1
            else:
                f_counts[tid] = f_counts[tid] + 1
            assignments.append([align.query_name, align.reference_name])
        if not align.reference_name:
            assignments.append([align.query_name, "-"])

    # create output df
    df_assignments = pd.DataFrame(assignments, columns=["query", "reference"])
    df_assignments["tax_id"] = df_assignments["reference"].apply(lambda x: x.split(":")[0])
    #df_assignments["name"] = df_assignments["tax_id"].apply(lambda x: dict_names[str(x)])

    results_df_full = freq_to_lineage_df(f_counts, f"{os.path.join(output_dir, filename)}_primary_counts", dict_nodes, dict_names)
    df_assignments.to_csv(f"{os.path.join(output_dir, filename)}_primary_assignments.tsv", sep='\t', index=False)
